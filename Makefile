UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

dev:
	rm -rf public/*
	@docker-compose down
	@docker-compose up -d

run:
	@docker-compose run --rm pandoc
