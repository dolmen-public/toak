# Toak

Traduction en français du livre "The art of kiltmaking".

[Lire la dernière version en ligne](https://dolmen-public.gitlab.io/toak/)

## Rédaction

Vous avez besoin de `docker` et `docker-compose`.

* Lancer le container `make dev`
* Connectez-vous sur http://localhost:8080

## [License](./LICENSE)

[Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/).

![Licence Creative Commons](./docs/cc_license.png)