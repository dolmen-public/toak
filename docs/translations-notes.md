# Notes de la traduction

Ce livre est traduit de la version anglaise [The art of kilt making](https://www.theartofkiltmaking.com/)
écrit par Barbara Tewksbury et Elsie Stuehmeyer (voir `::about-title`).

::: tip
Une fois le travail bien avancé, je contacterai les auteurs pour référencer ce travail.
:::

Cette traduction est publiée sous la license [Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/).

![Licence Creative Commons](./cc_license.png)

::: warning
Pour des raisons de copyright, les dessins et illustrations du livre original ne sont pas reproduits.
:::

\pagebreak

